# This function is not used. If it were, the use the thr_dpar should be reconsidered (___F I X M E___)
.get_pred_dens_from_GMM.Rmixmod <- function(X, # inferredVars 
                                            object, 
                                            tstat.obs, # 1-row matrix as otherwise more cases should be considered for cbind'ing
                                            log, 
                                            which, # "safe" version ignores, by correcting, spuriously high logL in area of low parameter density.
                                            solve_t_chol_sigma_lists=object$clu_params$solve_t_chol_sigma_lists, # added to allow optimization via iterative smmothing (which does not work)
                                            ...)  {
  if (is.null(dim(X))) {
    newjointX <- cbind(t(X),tstat.obs) # cbind two 1-row matrices
  } else {
    if (length(intersect(colnames(X),colnames(tstat.obs)))) stop("'X' should contain only inferred variables, not summary statistics")
    newjointX <- cbind(X,tstat.obs[rep(1,nrow(X)),,drop=FALSE]) # cbind two nrow(X)-row matrices
  }
  completedens <- predict(object$completedens,
                          newdata=newjointX,
                          solve_t_chol_sigma_list=solve_t_chol_sigma_lists$completedens,
                          logproportions=object$clu_params$logproportions,
                          clu_means=object$clu_params$completedens_means,
                          log=log,...)
  jointdens <- predict(object$jointdens,
                       newdata=newjointX, # latent vars will be ignored
                       solve_t_chol_sigma_list=solve_t_chol_sigma_lists$jointdens,
                       logproportions=object$clu_params$logproportions,
                       clu_means=object$clu_params$jointdens_means,
                       log=log,...) 
  if (log) {
    condvaldens <- completedens - jointdens
    if (which=="safe") {
      thr_info <- .get_thr_info(object)
      condvaldens <- condvaldens + pmin(0,jointdens-thr_info$thr_dpar)/thr_info$logdens_scaling # decreasing returned logL when parvaldens<thr_dpar
    }
  } else {
    condvaldens <- completedens/jointdens
    if (which=="safe") condvaldens <- condvaldens*pmin(1,jointdens/exp(object$thr_info$thr_dpar)) # odd use of same threshold on l as on logL. An exp() missing?
  }
  return(condvaldens) # vector if X was a matrix
}

# .get_pred_dens_from_GMM.Rmixmod(X=c(mu1=4,mu2=2,s2=1,latent=1), object=slik_j,tstat.obs=t(attr(slik_j$logLs,"stat.obs")), log=TRUE, which="raw")


# Sampling CIs creates artefacts in the clustering (covmats with large determinant <-> thin spikes in suraface) 
.sample_CIs <- function(
    object, refill, #cluster_args, 
    target_LR, 
    safe #  .safe_optim vs nlminb in .ad_hoc_opt; NOT controlling predict(., which)
) {
  CI_info <- allCIs(object,level = pchisq(target_LR,df=9), verbose=FALSE)
  profpts <-  CI_info$bounds
  if (nrow(profpts)>refill) profpts <- profpts[sample(nrow(profpts), refill),,drop=FALSE]
  #profpts <- .apply_par_constraints_lowup(profpts, object, fittedPars=fittedPars)
  profpts
}

# Maybe as suspect as .sample_CIs, for the same reason?
.sample_top_in_absol_constrs <- function(
    object, refill, target_LR, 
    fittedPars=object$colTypes$fittedPars,
    heuristic_fac=Infusion.getOption("sample_top_fac"), 
    verbose) {
  
  lower <- object$lower
  upper <- object$upper
  locrange <- upper-lower
  MSLE <- object$MSL$MSLE
  np <- length(fittedPars)
  profpts <- matrix(NA, nrow=2*np, ncol=np,dimnames = list(NULL,names(MSLE)))
  Y <- numeric(2*np)
  ii <- 0L
  diag_hess <- diag(object$MSL$hessian)
  names(diag_hess) <- fittedPars
  for (st in fittedPars) {
    if (diag_hess[st]>0) {
      heuristic <- sqrt(target_LR/(heuristic_fac*diag_hess[st]))
    } else heuristic <- 0.05*locrange[st]
    x <- MSLE
    #x[st] <- MSLE[st]-min(heuristic,0.99*(MSLE[st]-lower[st]))
    if (heuristic<0.99*(MSLE[st]-lower[st])) {
      x[st] <- MSLE[st]-heuristic
      xy <- profile(object, value= x[st])
      xx <- attr(xy,"solution")
      x[names(xx)] <- xx
      ii <- ii+1L
      profpts[ii,] <- x
      Y[ii] <- xy[1]
    }
    #
    #x[st] <- MSLE[st]+min(heuristic,0.99*(upper[st]-MSLE[st]))
    if (heuristic<0.99*(upper[st]-MSLE[st])) {
      x[st] <- MSLE[st]+heuristic
      xy <- profile(object, value= x[st])
      xx <- attr(xy,"solution")
      x[names(xx)] <- xx
      ii <- ii+1L
      profpts[ii,] <- x
      Y[ii] <- xy[1]
    } 
  }
  if (ii>0L) {
    Y <- Y[1:ii]
    profpts <- profpts[1:ii,,drop=FALSE]
    LRs <- object$MSL$maxlogL- Y
    profgood <- which(LRs < target_LR) # keep small positive values
    if (length(profgood)>refill) profgood <- profgood[sample(length(profgood), refill)]
    profpts <- profpts[profgood,,drop=FALSE]
    if (verbose) {
      LRs <- LRs[profgood]
      cat(paste0("Adding ", length(profgood), " points (LR info: mean= ", 
                 signif(mean(LRs),3), ", range= ",paste(signif(range(LRs),3),collapse="--"),")."))
    }
    profpts <- .apply_par_constraints_lowup(profpts, object, fittedPars=fittedPars,
                                            locrange=locrange, lower=lower,upper=upper)
    profpts
  } else NULL
}

# but I had .rparam_dMixmod_around_focal()... and anyway I am not using this
.sample_best_clu_in_absol_constrs.Rmixmod <- function(
    object, nsim,
    X=object$MSL$MSLE, # parameters only 
    clu_means=object$clu_params$pardens_means,
    solve_t_chol_sigma_list=object$clu_params$solve_t_chol_sigma_lists$pardens,
    sigma=object$pardens@parameters@variance # .@variance returns a list, .["variance",] returns an array
)  {
  if (is.null(dim(X))) {
    X <- t(X)
  } 
  nbClu <- ncol(clu_means)
  logdensities <- numeric(nbClu)
  for (clu_it in seq_len(nbClu)) {
    logdensities[clu_it] <- .fast_dmvnorm(
      X, mean=clu_means[, clu_it], solve_t_chol_sigma= solve_t_chol_sigma_list[[clu_it]],log=TRUE)
  }
  best_clu <- which.max(logdensities)
  trypoints <- rmvnorm(nsim, 
                       mean=clu_means[, best_clu], 
                       sigma= sigma[[best_clu]])
  # colnames(trypoints) <- object$colTypes$fittedPars # they are already here
  ## Handling box and 'constr_crits' constraints: 
  trypoints <- .apply_par_constraints_LOWUP(trypoints, object=object)
  trypoints
  
}

.old_calc_thr_dpar <- function(object, fittedPars=object$colTypes$fittedPars, level, pred_data=NULL) {
  ## Define, or redefine, threshold used by predict(., which="safe") to remove spurious high logL in parameters regions where it is poorly estimated (low parameter density)
  # Let us assume that the recent iteration(s) filled an interesting region: _F I X M E_ that is not so simple
  if (is.null(pred_data)) { # optional argument, to allow easier direct calls of this fn
    nr <- nrow(object$logLs)
    recent <- seq(max(1L,nr-1000L),nr)
    pred_data <- object$logLs[recent,fittedPars, drop=FALSE] # checks likelihood of all points to initiate maximization. Should be efficient
  }
  pardensv <- predict(object,newdata = pred_data, which="parvaldens", constr_tuning=FALSE) # penalty useless as pred_data satisfy constraints
  # Set the threshold for "safe" logL computation as function of parameter density:
  safelowerdens <- quantile(pardensv,probs=1/sqrt(length(pardensv)))
  npar <- length(fittedPars)
  target_LR <-  eval(.Infusion.data$options$target_LR_min_value) # the min_value in .process_target_LR
  extrapol_LR <- 2*target_LR
  thr_dpar <- max(max(pardensv)-extrapol_LR, safelowerdens) # ___F I X M E___ use high quantile instead of max for more stable threshold ? 
  # ___F I X M E___ rethink (but may be OK): 
  # impacts bounds of the few CIs that exclude a DGP.
  # An objective criterion is that the plot1Dprof should be smooth, and this was achieved by the
  # highest extrapolation ( extrapol_LR <- qchisq((1+level)/2, df=length(fittedPars)) ), and is 
  # currently achieved by moderate extrapol_LR and by the following logdens_scaling.
  #
  # An alternative approach would be to expand the sampling beyond the current target_LR, 
  # together with stricter (lower) logdens_scaling?
  logdens_scaling <- qchisq(level, df=npar)/qchisq(level, df=1)
  list(thr_dpar=thr_dpar,        # threshold value of parameter density
       logdens_scaling= logdens_scaling )
}

.essai_calc_pardens_thr_info <- function(object, fittedPars=object$colTypes$fittedPars, level, 
                                         pred_data=object$logLs[,fittedPars, drop=FALSE]) {
  logls <- predict(object,newdata = pred_data, which="lik", constr_tuning=FALSE)
  logls_order <- order(logls, decreasing = FALSE)
  qqn <- qqnorm(y=logls[logls_order], plot.it = FALSE)
  heuristic <- diff(qqn$y)/diff(qqn$x)
  safe_logls_ids <- logls_order[-1L][heuristic<1]
  maxlogl <- max(logls[safe_logls_ids])
  ## If I set thr_dpar <- predict(object,newdata = pred_data[whichmaxlogl,], which="parvaldens", ...)
  ## I see an artefact [see odp plot in devel/explanations]: profile values for several parameters tend to be the 'maxlogl' => identical p values for several SLRTs
  ## This occurs when pardens for the tested value is practically always below the dpar_thr. 
  ## * In that case any value with lik>maxlogl will be penalized, so the maximization is over
  ## parms with lik<maxlogl, and if lik>maxlogl easily at low densities, 
  ## we just get max(lik | lik<maxlogl) = maxlogl.
  ##
  ## * This artefact suggests we need more simul to increase parvaldens for tested value.
  ##
  ## Practical solution below it to allow lower thr_dpar by exploring the putative CI range
  # which_in_putative_CI <- which(logls> maxlogl-qchisq(level, df=1)/2)
  # thr_dpar <- min(predict(object,newdata = pred_data[which_in_putative_CI,], which="parvaldens", constr_tuning=FALSE))
  thr <- qchisq(level, df=1)/2 
  which_in_top_slice <- which(seq_along(logls) %in% safe_logls_ids & logls> maxlogl-thr)
  ## trivial protection, but not optimized in any sense:
  while (length(which_in_top_slice)<2L) {
    thr <- 2*thr
    which_in_top_slice <- which(seq_along(logls) %in% safe_logls_ids & logls> maxlogl-thr)
  }
  dpar_range <- range(predict(object,newdata = pred_data[which_in_top_slice,], which="parvaldens", constr_tuning=FALSE))
  #
  # If I instead penalized lik even when it is<maxlogl (how?), and assuming the MSLE has high pardens,
  # this will lower the profile values and the p-values.
  # Set the threshold for "safe" logL computation as function of parameter density:
  # list2env(
  list(thr_dpar=dpar_range[1], # sum(dpar_range*c(0.75,0.25)),        # threshold value of parameter density
       reft_maxlogl=maxlogl) 
  #  , 
  #        parent=emptyenv()) # envir so that it can be updated similarly to the the MSL envir
  ## Result is NOT modified when a new maximum is found, as it depends only on the logls of the reftable points
  ## Attempts to hack it differently were not successful and rather suggested fixing other issues (such as using
  ## nsteps in SLRT, or more deeply, by better parameter space exploration:
  ## B_13from17, replicate 49, fit by v2.1.106 vs 2.1.112)
}

# _F I X M E_ not used. Perhaps some idea but result not quite convincing.
.calc_filltop_weights_specul <- function(object, trypoints, 
                                         log_rel_weight_penal, 
                                         penal, # logical
                                         exploration_fac=.Infusion.data$options$exploration_fac, 
                                         target_LR, which) {
  logLs <- predict(object,trypoints,which=which, constr_tuning=FALSE) 
  
  weights <- - log_rel_weight_penal ## weights as opposite of penalizations
  
  lrs <- logLs-max(logLs) 
  weights <- weights-max(weights) 
  highw <- (weights > -target_LR)
  lowlrs <- (lrs < -2* target_LR)
  weights[highw] <- -target_LR  
  weights[lowlrs] <- pmin(-2* target_LR, weights)[lowlrs]
  weights <- exp(weights-max(weights)-target_LR) 
  
  flatnd_max <- max(logLs)-target_LR # lower logL value determined by target LR
  upperslice <- ( logLs> flatnd_max ) ## defines an upper slice of logLs values.
  
  list(weights=weights, freq_upperslice=sum(upperslice)/length(upperslice))
}

# Not totally bad for simple code. Distinct features are:
# * Logls/penalization balance, (but see lowerslice weights in basic procedure),
# * no correction of weight tails,
# top_pts
.calc_filltop_weights2 <- function(object, trypoints, 
                                   log_rel_weight_penal, 
                                   penal, # logical
                                   exploration_fac=.Infusion.data$options$exploration_fac, 
                                   target_LR, which) {
  logLs <- predict(object,trypoints,which=which, constr_tuning=FALSE) 
  weights <- logLs - 1.1* log_rel_weight_penal   # logLs - (z<1)*log_rel_weight_penal would be more like EI ?
  
  weights <- exp(weights-max(weights)) 
  
  ## Adding 'top_pts' drives initial upwards push of the cloud of points.
  ## Some sort of balance between upward and lateral expansions may berequired.
  top_pts <- head(order(logLs, decreasing=TRUE),
                  min(10, ceiling(length(weights)/10))) # beware number of trypoints >> target size
  logLs_u <- logLs[top_pts]
  weights[top_pts] <- pmax(weights[top_pts], exp(logLs_u-logLs_u[1]))
  
  # upperslice info is in the return value
  flatnd_max <- max(logLs)-target_LR # lower logL value determined by target LR
  upperslice <- ( logLs> flatnd_max ) ## defines an upper slice of logLs values.
  
  list(weights=weights, freq_upperslice=sum(upperslice)/length(upperslice))
}




