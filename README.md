[![CRAN](http://www.r-pkg.org/badges/version-ago/Infusion)](https://cran.r-project.org/web/packages/Infusion)

`Infusion` is an R package for ***Inf***erence ***us***ing simulat***ion***, available on CRAN. This repository hosts the development version. One can install it by running
`remotes::install_gitlab(repo = "francois/Infusion", subdir = "package", host = "gitlab.mbb.univ-montp2.fr")`. 

In recent years, simulation-based inference methods such as approximate Bayesian computation (ABC) have extensively been used to infer parameters of population genetic models where the likelihood is intractable. 
`Infusion` implements an alternative approach, summary likelihood, that provides a likelihood-based analysis of the information retained in the summary statistics whose distribution is simulated. 
A small simulation study with the first version of the package ([Rousset et al, 2017](https://onlinelibrary.wiley.com/doi/10.1111/1755-0998.12627)) 
showed that this approach can provide confidence intervals with better controlled coverage, independently of a prior distribution on parameters. 
The current version, whose methods are described [here](https://www.biorxiv.org/content/10.1101/2024.09.30.615940v1), is much more efficient (requiring fewer simulations).   

To get started, see the [gentle introduction](/documents/InfusionIntro.pdf). 
 
